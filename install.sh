#!/bin/sh

profile=$1

for f in $(find ./dot/ -type f); do
    src="$(realpath $f)"
    dst="$HOME/.$(basename $f)"

    ln -sf $src $dst
done

for f in $(find ./bin/ -type f); do
    src="$(realpath $f)"
    dst="$HOME/.local/bin/$(basename $f)"

    ln -sf $src $dst
done

for f in $(find ./local/ -type f); do
    src="$(realpath $f)"
    dst="$HOME/.$(basename $f).local"

    ln -sf $src $dst
done
