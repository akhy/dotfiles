#!/bin/sh

gcfg() {
    git config --global "$1" "$2"
}

git config --global color.ui auto
git config --global color.ui auto

gcfg color.ui auto
gcfg pretty.nice '%C(yellow)%h%C(reset) %C(white)%s%C(cyan)%d%C(reset) -- %an; %ar'

gcfg alias.lo 'log --pretty=nice'
gcfg alias.b 'branch'
gcfg alias.ba 'branch -a'
gcfg alias.ci 'commit'
gcfg alias.co 'checkout'
gcfg alias.d 'diff'
gcfg alias.dc 'diff --cached'
gcfg alias.fp 'format-patch'
gcfg alias.g '!git gui &'
gcfg alias.gr 'log --graph'
gcfg alias.go 'log --graph --pretty=oneline --abbrev-commit'
gcfg alias.k '!gitk &'
gcfg alias.ka '!gitk --all &'
gcfg alias.lc 'log ORIG_HEAD.. --stat --no-merges'
gcfg alias.lp 'log --patch-with-stat'
gcfg alias.mnff 'merge --no-ff'
gcfg alias.mt 'mergetool'
gcfg alias.p 'format-patch -1'
gcfg alias.serve '!git daemon --reuseaddr --verbose  --base-path=. --export-all ./.git'
gcfg alias.sra 'svn rebase --all'
gcfg alias.sh '!git-sh'
gcfg alias.st 'status'
gcfg alias.stm 'status --untracked=no'
gcfg alias.stfu 'status --untracked=no'
gcfg alias.pullsrb '!git stash save && git pull --rebase && git stash pop && echo \"Success!\"'
gcfg alias.mffo 'merge --ff-only'

gcfg alias.personal 'config user.email akhy@chickenzord.com'
gcfg alias.work 'config user.email akhyar.amarullah@go-jek.com'
